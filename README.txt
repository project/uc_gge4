Welcome to UC Global Gateway e4.

Installation
============
1. Copy the uc_global_gateway_e4 directory into your web site's
 /sites/modules directory.
2. Activate the module on the modules page.
3. Enable and configurate the payment method on the
settings page admin/store/settings/payment/method/credit.
4. Choose "Transaction mode" before using.

Requirements
============
Ubercart.
