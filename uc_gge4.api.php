<?php

/**
 * @file
 * Hooks provided by the UC Global Gateway e4 module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Allows transaction data to be altered or add custom before sending.
 *
 * @param array $data
 *   The transaction data as specified by the Global Gateway e4 API.
 */
function hook_uc_gge4_transaction_alter(&$data) {
  $data['description'] = 'Custom field with transaction description.';
}

/**
 * @} End of "addtogroup hooks".
 */
